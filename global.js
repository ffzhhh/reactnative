import { Dimensions, PixelRatio } from 'react-native';

const dp2px = dp => PixelRatio.getPixelSizeForLayoutSize(dp);
let designSize = { width: 750 };
let pxRatio = PixelRatio.get();
let win_width = Dimensions.get("window").width;
let win_height = Dimensions.get("window").height;
let width = dp2px(win_width);
let height = dp2px(win_height);
let design_scale = designSize.width / width;
height = height * design_scale
let scale = 1 / pxRatio / design_scale;


global.width = width;
global.height = height;
global.scale = scale;



// import {Dimensions} from 'react-native'
// // 设计图尺寸是750
// const width = Dimensions.get('window').width;
// global.p2d = (size) => size*width/750;