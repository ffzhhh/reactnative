import { useState, useEffect } from "react";
import axios from 'axios';

const baseUrl = 'https://cnodejs.org/api/v1';

const cnode = (tab) => {
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        // 发起网络请求获取数据
        axios.get(`${baseUrl}/topics`, {
            params: {
                tab: tab,
                page: 1
            }
        })
        .then(response => {
            // 数据获取成功，设置列表数据并关闭加载状态
            console.log(response.data);
            setList(response.data.data);
            setLoading(false);
        })
        .catch(error => {
            // 数据获取失败，输出错误并关闭加载状态
            console.error('Error fetching data:', error);
            setLoading(false);
        });
    }, [tab]);

    return [list, loading];
};

export default cnode;