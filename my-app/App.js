import { useState,useEffect } from 'react';
import { 
    StatusBar, StyleSheet, TextInput, TouchableOpacity,
    Text, View, Image, ImageBackground,
    ScrollView, Button, Pressable, FlatList
} from 'react-native';
import useTopics from './src/hooks/use-topics.js';
export default function App() {
    const [value, setValue] = useState('');
    const { loading, list, onRefresh, onEndReached } = useTopics()
    return (
        <View style={{flex: 1}}>
            <TextInput
                value={value}
                onChangeText={text=>setValue(text)}
                style={{
                    borderWidth: 2,
                    borderColor: '#f00',
                    width: 300,
                    height: 30,
                    borderRadius: 15,
                    alignSelf: 'center',
                    margin: 20,
                    paddingHorizontal: 20
                }}
            />
            <FlatList 
                refreshing={loading}
                onRefresh={onRefresh}
                onEndReached={onEndReached}
                style={{flex: 1}}
                data={list}
                renderItem={({item})=><Text>{item.title}</Text>}
                keyExtractor={(item)=>item.id}
            />
            <StatusBar />
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ccc',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
