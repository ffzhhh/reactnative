import React, { useState } from 'react';
import {
    View,
    TextInput,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import cnode from '../api/cnode';

export default function App() {
    const [tab, setTab] = useState('');
    const [list, loading] = cnode(tab);
    const [value, setValue] = useState('');

    const getTimeDistance = (lastReplyAt) => {
        const currentTime = new Date();
        const timestamp = new Date(lastReplyAt);
        const diff = Math.floor((currentTime - timestamp) / 1000); // Time difference in seconds

        if (diff < 60) {
            return 'Just now';
        } else if (diff < 3600) {
            return Math.floor(diff / 60) + '分钟前';
        } else if (diff < 86400) {
            return Math.floor(diff / 3600) + '小时前';
        } else if (diff < 2592000) {
            return Math.floor(diff / 86400) + '天前';
        } else if (diff < 31104000) {
            return Math.floor(diff / 2592000) + '个月前';
        } else {
            return Math.floor(diff / 31104000) + '年前';
        }
    };

    return (
        <View>
            <View style={styles.upper}>
                <View style={styles.piccontainer}>
                <Image
                    style={styles.picture}
                    source={{
                        uri: 'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png'
                    }}
                />
                </View>
                <TextInput
                    value={value}
                    onChangeText={text => setValue(text)}
                    style={styles.search}
                />
                <View style={styles.nav}>
                    <Text style={styles.classfiy}>首页</Text>
                    <Text style={styles.classfiy}>未读消息</Text>
                    <Text style={styles.classfiy}>新手入门</Text>
                    <Text style={styles.classfiy}>API</Text>
                    <Text style={styles.classfiy}>关于</Text>
                    <Text style={styles.classfiy}>设置</Text>
                    <Text style={styles.classfiy}>退出</Text>
                </View>
            </View>
            <View style={styles.contain}>
                <View style={styles.containerr}>
                    <TouchableOpacity
                        style={[styles.btn, tab === 'all' ? { backgroundColor: '#77BD00' }:null]}
                        onPress={() => setTab('all')}
                    >
                        <Text style={[styles.word, tab === 'all' ? { color: 'white' } : null]}>全部</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.btn, tab === 'good' ? { backgroundColor: '#77BD00' }:null]}
                        onPress={() => setTab('good')}
                    >
                        <Text style={[styles.word, tab === 'good' ? { color: 'white' } : null]}>精华</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.btn, tab === 'share' ? { backgroundColor: '#77BD00' }:null]}
                        onPress={() => setTab('share')}
                    >
                        <Text style={[styles.word, tab === 'share' ? { color: 'white' } : null]}>分享</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.btn, tab === 'ask' ? { backgroundColor: '#77BD00' }:null]}
                        onPress={() => setTab('ask')}
                    >
                        <Text style={[styles.word, tab === 'ask' ? { color: 'white' } : null]}>问答</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.btn, tab === 'job' ? { backgroundColor: '#77BD00' }:null]}
                        onPress={() => setTab('job')}
                    >
                        <Text style={[styles.word, tab === 'job' ? { color: 'white' } : null]}>招聘</Text>
                    </TouchableOpacity>
                    {/* onPress={() => setTab('all')}` 点击按钮后调用 `setTab` 函数来更新当前选中的分类为 'all' */}
                </View>
                {loading ? <Text>Loading...</Text> :
                    list.map(item => (
                        <View style={styles.con} key={item.id}>
                            <Image source={{ uri: item.author.avatar_url }} style={styles.avatar} />
                            <Text style={styles.sharea}> {item.tab === 'ask' ? '问答' : '分享'}</Text>
                            <Text style={styles.count}>{item.reply_count} / {item.visit_count}</Text>
                            <Text style={styles.title}>{item.title.length > 17 ? item.title.substring(0, 17) + '...' : item.title}</Text>
                            <Text style={[styles.time, { position: 'absolute', right: 4 }]}>{getTimeDistance(item.last_reply_at)}</Text>
                        </View>
                    ))
                }
            </View>
        </View>

    );
};

const styles = StyleSheet.create({
    upper: {
        flexDirection: 'column',
        alignItems: 'center',
        padding: 7,
        backgroundColor: '#444',
    },
    // piccontainer:{
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     width: 400,
    //     height: 40,
    // },
    picture: {
        width: 300,
        height: 140,
    },
    search: {
        height: 30,
        width:300,
        paddingHorizontal: 10,
        backgroundColor: 'grey',
        borderRadius: 7,
        marginHorizontal: 10,
    },
    nav: {
        alignItems: 'center', 
        flexDirection: 'row',
        padding: 4, // 添加整体内边距
        flexWrap: 'wrap', 
      },
    classfiy: {
        marginBottom: 4, // 添加垂直间距
        padding:9,
        color:'#D3D3D3',
        fontWeight:'400' ,
        fontSize:16,
      },
      time:{
        color:'grey'
      },
    contain: {
        padding: 10,
    },
    containerr: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 10,
    },
    btn: {
        padding: 5,
        borderRadius:4
       
    },
    word: {
        fontSize: 16,
        color:'#77BD00'
    },
    con: {
        padding: 10,
        backgroundColor: '#fff',
        borderRadius: 5,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    avatar: {
        width: 30,
        height: 30,
        borderRadius: 4,
        marginRight: 10,
    },
    tab: {
        backgroundColor: '#ccc',
        padding: 5,
        borderRadius: 5,
        marginRight: 10,
        color: '#333',
    },
    sharea:{
        backgroundColor: 'lightgrey',
        borderRadius: 2,
        marginRight: 7,
        color: 'grey',
    },
    count:{
        fontSize:7,
        color:'grey',
        margin:7
    }
   
});


