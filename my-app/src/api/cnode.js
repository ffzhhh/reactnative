import axios from './axios';

export const getTopics = params => axios.get('/topics', {params})
export const postTopics = data => axios.post('/topics', data)

// export const getTopics2 = (params) => {
//     return axios.get('/topics', {params})
// }