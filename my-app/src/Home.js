import { StyleSheet, Text, View, PixelRatio, Dimensions } from 'react-native';

const dp2px = dp => PixelRatio.getPixelSizeForLayoutSize(dp);
let designSize = { width: 750 };
let pxRatio = PixelRatio.get();
let win_width = Dimensions.get("window").width;
let win_height = Dimensions.get("window").height;
let width = dp2px(win_width);
let height = dp2px(win_height);
let design_scale = designSize.width / width;
height = height * design_scale
let scale = 1 / pxRatio / design_scale;

const Home = () => {
    return (
        <View>
            <Text>hello</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        width: width,
        height: height,
        transform: [
            { translateX: -width * .5 },
            { translateY: -height * .5 },
            { scale: scale },
            { translateX: width * .5 },
            { translateY: height * .5 }
        ]
    }
});
export default Home