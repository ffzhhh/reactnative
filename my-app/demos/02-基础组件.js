import { useState } from 'react';
import {
    StatusBar, StyleSheet, TextInput, TouchableOpacity,
    Text, View, Image, ImageBackground,
    ScrollView, Button
} from 'react-native';

// 布局元素默认设置了display: flex
// 布局元素默认设置了position: relative
export default function App() {
    const [value, setValue] = useState('')
    return (
        <ScrollView>
            <TouchableOpacity
                style={{
                    backgroundColor: '#ccc',
                    width: 200,
                    height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 25,
                    marginVertical: 30,
                    alignSelf: 'center'
                }}
            >
                <Text style={{
                    fontSize: 20,
                    color: '#f00'
                }}>hello</Text>
            </TouchableOpacity>
            <Button
                title="按钮"
                color="#f00"
            />
            <TextInput
                value={value}
                onChangeText={text => setValue(text)}
                style={{
                    borderWidth: 2,
                    borderColor: '#f00',
                    width: 300,
                    height: 30,
                    borderRadius: 15,
                    alignSelf: 'center',
                    margin: 20,
                    paddingHorizontal: 20
                }}
            />
            {/* <ImageBackground
                style={{width: 200,height: 100}}
                source={require('./assets/icon.png')}
            >
                <Text>hello</Text>
            </ImageBackground> */}
            {/* <Image
                style={{
                    width: 100,
                    height: 100,
                    position: 'absolute'
                }}
                source={require('./assets/icon.png')}
            /> */}
            {/* <Image
                style={{
                    width: 200,height: 200,
                    resizeMode: 'repeat'
                }}
                source={require('./assets/favicon.png')}
            /> 
            <Image 
                style={{
                    resizeMode: 'contain',
                    width: 300,
                    height: 100,
                    borderColor: '#f00',
                    borderWidth: 2
                }}
                source={{
                    uri: 'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png'
                }}
            /> */}
            <StatusBar />
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ccc',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
