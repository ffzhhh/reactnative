import { useState } from 'react';
import {
    StatusBar, StyleSheet, TextInput, TouchableOpacity,
    Text, View, Image, ImageBackground,
    ScrollView, Button, Pressable, FlatList
} from 'react-native';
const Btn = ({ onPress, children, style, textStyle }) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={
                [
                    {
                        padding: 10,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#ccc'
                    },
                    style
                ]
            }
        >
            <Text style={[{
                fontSize: 20,
                color: '#f00'
            }, textStyle]}>{children}</Text>
        </TouchableOpacity>
    )
}
const DATA = [
    {
        id: '1',
        title: 'First Item',
    },
    {
        id: '2',
        title: 'Second Item',
    },
    {
        id: '3',
        title: 'Third Item',
    },
];
export default function App() {
    const [value, setValue] = useState('')
    return (
        <View>
            <View style={{
                flexDirection: 'row'
            }}>
                <Btn
                    onPress={() => console.log(123)}
                    textStyle={{ color: '#00f' }}
                    style={{
                        width: 100,
                        borderRadius: 10,
                        backgroundColor: '#0f0'
                    }}
                >全部</Btn>
                <Btn>精华</Btn>
                <Btn>分享</Btn>
            </View>
            <Pressable
                android_ripple={{ color: '#f00' }}
                style={{ backgroundColor: '#ccc' }}
            // style={({pressed})=>({
            //     backgroundColor: pressed?'#ccc':'#fff'
            // })}
            >
                <Text >
                    Pressable
                </Text>
            </Pressable>
            <TextInput
                value={value}
                onChangeText={text => setValue(text)}
                style={{
                    borderWidth: 2,
                    borderColor: '#f00',
                    width: 300,
                    height: 30,
                    borderRadius: 15,
                    alignSelf: 'center',
                    margin: 20,
                    paddingHorizontal: 20
                }}
            />
            <FlatList
                data={DATA}
                renderItem={({ item }) => <Text>{item.title}</Text>}
                keyExtractor={(item) => item.id}
            />
            <StatusBar />
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ccc',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
