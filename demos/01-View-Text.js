import { StatusBar, StyleSheet, Text, View } from 'react-native';

// native中布局元素默认就是弹性盒
// 不需要再去设置display: flex
// 弹性盒的主轴方向是纵向
// View不能加onPress事件的
export default function App() {

    return (
        <View style={styles.container}>
            <Text
                style={styles.textColor}
            >
                hello 3333
                {/* Text 可加事件 */}
                {/* Text不能继承外层View的字体相关样式 */}
                {/* 能够继承父级Text组件的字体样式 */}
                <Text>R N</Text>
            </Text>
            <Text>混合应用开发</Text>
            <StatusBar />
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ccc',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textColor: {
        color: '#f00',
        fontSize: 30
    }
});
