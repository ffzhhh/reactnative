import axios from './axios';

export const getTopics = params => axios.get('/topics', {params})
export const postTopics = data => axios.post('/topics', data)

// export const getTopics2 = (params) => {
//     return axios.get('/topics', {params})
// }
// // cnode.js
// import { useState, useEffect } from "react";
// import axios from 'axios';

// const baseUrl = 'https://cnodejs.org/api/v1';

// const cnode = (tab) => {
//     const [list, setList] = useState([]);
//     const [loading, setLoading] = useState(false);

//     const postTopic = (data) => {
//         axios.post(`${baseUrl}/topics`, data)
//             .then(response => {
//                 console.log(response.data);
//             })
//             .catch(error => {
//                 console.error('Error posting topic:', error);
//             });
//     };

//     useEffect(() => {
//         setLoading(true);
//         axios.get(`${baseUrl}/topics`, {
//             params: {
//                 tab: tab,
//                 page: 1
//             }
//         })
//         .then(response => {
//             console.log(response.data);
//             setList(response.data.data);
//             setLoading(false);
//         })
//         .catch(error => {
//             console.error('Error fetching data:', error);
//             setLoading(false);
//         });
//     }, [tab]);

//     return [list, loading, postTopic];
// };

// export default cnode;