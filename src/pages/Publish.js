import React, { useState } from 'react';
import { View, Text, Modal, Button,  StyleSheet,TextInput, TouchableOpacity } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import axios  from 'axios';
const Publish = ({ visible, onClose }) => {
// const [selectedType, setSelectedType] = useState('ask');

const [tab,setTab] = useState('');
const [title,setTitle] = useState('');
const [inputText,setInputText] = useState('');

const publishContent = () => {
    if (!tab || !title || !inputText) {
        console.log("请填写完整信息");
        return;
    }
    console.log(tab, title, inputText);
    axios.post(`https://cnodejs.org/api/v1/topics`, {
        tab: tab,
        title: title,
        content: inputText,
        accesstoken: " 5c1953e2-5f7d-4079-8cf1-307cfe6802fe"
    },{
        headers:{
            'Content-Type':'application/json'
        }
    }).then((res) => {
        console.log(res);
        console.log("发布成功")
        alert("发布成功");
        onClose();
    }).catch((err) => {
        console.log(err);
        console.log("发布失败");
        alert("发布失败");
    });
};
    return (
        <Modal
            visible={visible}
            animationType="slide"
            onRequestClose={onClose}
            transparent={true}
        >
            <View style={styles.modalContent}>
               
                <RNPickerSelect
            onValueChange={(value) => setTab(value)}
            items={[
                { label: '问答', value: 'ask' },
                { label: '分享', value: 'share' },
                { label: '招聘', value: 'job' },  
                { label: '客户端测试', value: 'dev' }, 
                ]}
        />
             {/* Title Input Field */}
            <TextInput
               style={styles.title} placeholder='输入标题' value={title} onChangeText={setTitle}
            /> 
            
            {/* Content Input Field */}
            <TextInput 
               style={styles.title2} placeholder='输入内容' value={inputText} onChangeText={setInputText}
               multiline={true} numberOfLines={14}
            />   
            <View style={styles.butcontainer}>
                <TouchableOpacity title="" onPress={publishContent} style={styles.pubbutton}>
                    <Text style={styles.pub}>发布</Text>
                </TouchableOpacity>
                <TouchableOpacity title="" onPress={onClose} style={styles.pubbutton}>
                    <Text style={styles.pub}>关闭</Text>
                </TouchableOpacity>
            </View>
            
            </View>
          
        </Modal>
    );
};

const styles = StyleSheet.create({
    modalContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F8F8FF',
        padding: 20,
        borderRadius: 10,
        margin: 50,
    },
    picker: {
        width: 200,
        height: 50,
        backgroundColor: '#f0f0f0',
        marginBottom: 20,
    },
    pubbutton:{
        width:60,
        height:30,
        backgroundColor:'#3CB371',
        textAlign:'center',
        color:'white',
        margin:17

    },
    title:{
        borderColor:'#BEBEBE',
        borderWidth:1,
        width:'100%',
        height:40,
        margin:10,
        borderRadius:3,
        color:'black',
        padding:4

    },
    title2:{
        borderColor:'#BEBEBE',
        borderWidth:1,
        width:'100%',
        height:300,
        margin:10,
        borderRadius:3,
        color:'black',
        padding:4,


    },
    butcontainer:{
        flexDirection:'row',
        justifyContent:'center'
    },
    pub:{
        textAlign:'center',
        color:'white',

    }

    
});

export default Publish;