import { useState } from 'react';
import { StyleSheet, Text, View, FlatList, TextInput, Button } from 'react-native';
import useTopics from '../hooks/use-topics';

const Home = ({navigation}) => {
    const [value, setValue] = useState('');
    const { loading, list, onRefresh, onEndReached } = useTopics();
    return (
        <View style={{flex: 1}}>
            <Button 
                title='to detail'
                onPress={() => navigation.navigate('detail', { data: 'pass data' })}
            />
            <TextInput
                value={value}
                onChangeText={text => setValue(text)}
                style={{
                    borderWidth: 2,
                    borderColor: '#f00',
                    width: '50%',
                    height: 50,
                    borderRadius: 25,
                    alignSelf: 'center',
                    margin: 20,
                    paddingHorizontal: 20
                }}
            />
            {/* <FlatList 
                refreshing={loading}
                onRefresh={onRefresh}
                onEndReached={onEndReached}
                style={{flex: 1}}
                data={list}
                renderItem={({item})=><Text>{item.title}</Text>}
                keyExtractor={(item)=>item.id}
            /> */}
        </View>
    )
}
const styles = StyleSheet.create({

});
export default Home