import { StatusBar, Text, StyleSheet, View } from 'react-native';
import './global';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator();
const Stack2 = createNativeStackNavigator();
const Stack3 = createNativeStackNavigator();

const Home = ({navigation}) => {
    const goDetail = () => {
        navigation.navigate('detail')
    }
    return <Text onPress={goDetail}>首页</Text>
}
const Detail = () => <Text>详情eee</Text>

export default function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerStyle: {
                        backgroundColor: '#f4511e',
                    },
                    // headerBackVisible: false,
                    // headerShown: false,
                    headerTitleAlign: 'center',
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                    },
                }}
            >
                <Stack.Screen
                    name='home'
                    component={Home}
                    options={{
                        title: '首页'
                    }}
                />
                <Stack.Screen
                    name='detail'
                    component={Detail}
                />
            </Stack.Navigator>
            <StatusBar />
        </NavigationContainer>
    );
}



const styles = StyleSheet.create({
    container: {
        width: width,
        height: height,
        transform: [
            { translateX: -width * .5 },
            { translateY: -height * .5 },
            { scale: scale },
            { translateX: width * .5 },
            { translateY: height * .5 }
        ]
    }
});

// import React, { useState } from 'react';
// import { 
//     View, 
//     ScrollView,
//     TouchableOpacity, 
//     Text,
//     StyleSheet
// } from 'react-native';

// import Cnode from './src/pages/Cnode'
// import Publish from './src/pages/Publish';

// export default function App() {
//     const [modalVisible, setModalVisible] = useState(false);

//     // const handlePublish = () => {
//     //     setModalVisible(true);
//     // };
    
//     // const handleModalClose = () => {
//     //     setModalVisible(false);
//     // };

//     return (
//         <View style={{flex: 1}}>
//             <ScrollView style={{width:'100%',height:'100%'}}>
//                 <Cnode />
//                 </ScrollView>
//             <View style={styles.container1}>
//             <TouchableOpacity onPress={() => setModalVisible(true)} style={styles.button}>
//                     <Text>发布</Text>
//                 </TouchableOpacity>
//             </View>
//             <Publish
//                 visible={modalVisible}
//                 onClose={()=>setModalVisible(false)}
//             />
//         </View>
//     );
// }

// const styles = StyleSheet.create({
//   container1: {
//     width:30,
//     position: 'absolute',
//     bottom: 50,
//     left: 440,
//     right: 0,
//   },
//   button: {
//     height: 60,
//     backgroundColor: '#9acd32',
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   modalContent: {
//     flex: 1, 
//     justifyContent: 'center', 
//     alignItems: 'center',
//     backgroundColor: '#FFFFFF',
//     padding: 20,
//     borderRadius: 10,
//     margin: 50,
// },
// })